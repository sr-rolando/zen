package httpd

import "testing"

var testDates = []struct {
	in   string
	want string
}{
	// The magic Go date
	{"2006-01-02T15:04:05-07:00", "02.01.2006 23:04"},
	// Unix start date
	{"1970-01-01T00:00:00+01:00", "01.01.1970 00:00"},
	// Day before Unix start date
	{"1969-12-31T12:12:12+01:00", "31.12.1969 12:12"},
	// Some futuristic date
	{"3003-03-03T03:03:03+01:00", "03.03.3003 03:03"},
	// Some current winter date
	{"2020-02-20T20:20:20+01:00", "20.02.2020 20:20"},
	// Some current summer date
	{"2020-08-08T08:08:08+02:00", "08.08.2020 08:08"},
	// Some invalid date
	{"2020-08-18T", ""},
}

func TestFormatDate(t *testing.T) {
	for _, date := range testDates {
		out := formatDate(date.in)
		if out != date.want {
			t.Errorf("Wrong date format, wanted %s instead of %s.", date.want, out)
		}
	}
}
