package httpd // import "zen/httpd"

import (
	"context"
	"fmt"
	"html/template"
	"io/ioutil" // TempFile
	"log"
	"net/http"
	"strconv"
	"time"

	"zen/activities"
	"zen/database"
	"zen/tcx"
	"zen/version"

	"github.com/gorilla/mux" // Define explicit routes for GET and POST
	"github.com/jackc/pgx/v4"
)

// Env is wrapping the database connection.
// All our handlers are methods on this struct.
// see No 3 in: https://www.alexedwards.net/blog/organising-database-access
type Env struct {
	db *pgx.Conn
}

// DashboardData holds everything that we pass on to the dashboard template.
type DashboardData struct {
	Message    string
	Activities []activities.ZenActivity
}

// RunServer defines our routes and starts the httpd server.
func RunServer(dsn string) {
	// Connect to database and make sure its schema is up-to-date.
	// Note: This will cause trouble if multiple instances of this app are run,
	//		i.e. via container orchestration.
	// 		In such case, database migrations should be run manually.
	db, err := database.OpenDb(dsn)
	panicOnError(err)
	defer database.ShutdownDb(db)
	database.PrintDbVersion(db)
	database.EnsureSchemaIsUpToDate(db)

	// Create an instance of Env that holds our database connection
	// for use throughout our handlers.
	env := &Env{db: db}

	// Setup routing endpoints.
	r := mux.NewRouter()

	// Make sure to handle static files (css, js, img) as well.
  // FIXME: Use /assets instead.
	fs_static := http.FileServer(http.Dir("./static/"))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs_static))
  fs := http.FileServer(http.Dir("./assets/"))
  r.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", fs))

	r.HandleFunc("/", env.startPage).Methods(http.MethodGet)
	r.HandleFunc("/", env.handleLogin).Methods(http.MethodPost)
	r.HandleFunc("/dashboard", env.dashboardPage).Methods(http.MethodGet)
	r.HandleFunc("/upload", env.postFileUpload).Methods(http.MethodPost)
	r.HandleFunc("/edit/{activityId}", env.handleGetEditActivity).Methods(http.MethodGet)
	r.HandleFunc("/activities/{activity}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		activity := vars["activity"]
		// get activity
		// show its details
		log.Print("Not implemented, yet: showing details for activity with id ", activity)
	}).Methods(http.MethodGet)

	log.Printf("Starting Zen v%s at http://localhost:1234/ ...\n", version.Version)
	log.Fatal(http.ListenAndServe(":1234", r))
}

func (env *Env) startPage(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/login.html")
	if err != nil {
		log.Print("template parse error: ", err)
	}
	err = t.Execute(w, nil) // nil - we don't have any data to pass to the login page
	if err != nil {
		log.Print("template execute error: ", err)
	}
}

// Handles form POST of /
func (env *Env) handleLogin(w http.ResponseWriter, r *http.Request) {
	// We don't validate anything, yet. Just redirect to the dashboard via 303 (see other).
	http.Redirect(w, r, "/dashboard", http.StatusSeeOther)
}

// Displays /dashboard
func (env *Env) dashboardPage(w http.ResponseWriter, r *http.Request) {
	dd := DashboardData{Message: ""}
	var err error
	// Retrieve up to ten most recent activities to be displayed on the dashboard.
	dd.Activities, err = activities.ReadActivities(env.db, 10)
	if err != nil {
		// Not much to do here. If we didn't retrieve any activities, we simply don't show any.
		// Just make sure to cleanly do so by rendering an empty array.
		dd.Activities = []activities.ZenActivity{}
		log.Println("  ..set up some empty activity set.")
	}

	log.Printf("rendering dashboard with %d activities\n", len(dd.Activities))
	dd.Message = fmt.Sprintf("There are currently %d activities.", len(dd.Activities))
	renderDashboard(w, dd)
}

// TODO Call this via Axios / REST / Svelte component, return either OK or notification message.
func (env *Env) postFileUpload(w http.ResponseWriter, r *http.Request) {
	log.Println("File upload endpoint.")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("uploadFile")
	if err != nil {
		log.Printf("Couldn't retrieve upload file: %v", err)
		// TODO add some return message to display to the user (as a toast or error msg div).
		return
	}
	defer file.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Error reading file: %v", err)
	}
	activityId, err := env.processByteArray(fileBytes)
	if err != nil {
		log.Printf("Error processing file: %v", err)
		// Notify frontend.
		dd := DashboardData{Message: ""}
		dd.Message = fmt.Sprintf("Could not process file '%s'. Error: %s.", handler.Filename, err)
		renderDashboard(w, dd)
	}
	// Redirect to /edit/<id>, allowing the user to add/edit some fields and set status = confirmed.
	redirectToUri := fmt.Sprintf("/edit/%d", activityId)
	http.Redirect(w, r, redirectToUri, http.StatusSeeOther)
}

// ProcessByteArray takes a byte array of TCX data, reads it in, and persists it to the database.
// Returns the id of the `activity` row in the db to allow user to edit activitie's data.
func (env *Env) processByteArray(byteArray []byte) (int64, error) {
	// Parse TCX
	tcxRoot, err := tcx.ReadFromByteArray(byteArray)
	if err != nil {
		return -1, err
	}

	// Persist to db.
	activityId, err := tcx.PersistInDb(env.db, tcxRoot)
	if err != nil {
		return -1, err
	}

	// Be done. Client notification etc. is handled by upstream.
	return activityId, nil
}

// Used by different handlers to render the dashboard page.
func renderDashboard(w http.ResponseWriter, dashboardData DashboardData) {
	t, err := template.New("dashboard.html").Delims("[[", "]]").ParseFiles("templates/dashboard.html")
	if err != nil {
		log.Print("template parse error: ", err)
	}
	err = t.Execute(w, dashboardData)
	logError(err, "template execute error")
}

// GET /edit/<id> shows the activity details and allows the user to change everything all around.
func (env *Env) handleGetEditActivity(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	activityId, err := strconv.ParseInt(params["activityId"], 10, 64)
	if err != nil {
		log.Printf("Error reading activity id from URL: %v. (Rendering dashboard instead.)", err)
		http.Redirect(w, r, "/dashboard", http.StatusSeeOther)
	}

	// Read data into an activities.ZenActivity.
	log.Printf("Reading activity with id=%d from database...", activityId)
	sql := "SELECT title, start_time, end_time, distance, pace, heart_rate_max FROM activity WHERE id = $1 ORDER BY id LIMIT 1"
	var title string
	var start_time time.Time
	var end_time time.Time
	var distance float64
	var pace float32
	var heart_rate_max int16
	err = env.db.QueryRow(context.Background(), sql, activityId).Scan(&title, &start_time, &end_time, &distance, &pace, &heart_rate_max)
	if err != nil {
		log.Printf("  ..error reading activity => %v", err)
		// Proceed with some defaults.
		title = "Placeholder"
		start_time = time.Now()
		end_time = time.Now()
		distance = 0
		pace = 0
		heart_rate_max = 0
	}
	activity := activities.ZenActivity{
		ID:             activityId,
		Title:          title,
		StartTime:      start_time,
		EndTime:        end_time,
		DistanceMeters: distance,
		Pace:           pace,
		HRmax:          heart_rate_max,
	}
	t, err := template.ParseFiles("templates/edit.html")
	if err != nil {
		log.Print("template parse error: ", err)
	}
	err = t.Execute(w, activity)
	if err != nil {
		log.Print("template execute error: ", err)
	}
}

// Returns a local date/time string for a given ISO 8601 time string.
func formatDate(iso8601date string) string {
	ref := "2006-01-02T15:04:05-07:00"
	t, err := time.Parse(ref, iso8601date)
	if err != nil {
		return ""
	}
	loc, _ := time.LoadLocation("Europe/Berlin")
	return t.In(loc).Format("02.01.2006 15:04")
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}

// Do nothing with the error besides logging it.
func logError(err error, prefix string) {
	if err != nil {
		log.Printf("%s: %v", prefix, err)
	}
}
