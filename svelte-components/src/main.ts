import FileUpload from './FileUpload.svelte';
import EditActivity from './EditActivity.svelte';

new FileUpload({
  target: document.querySelector('#svelte-file-upload-container'),
});
new EditActivity({
  target: document.querySelector('#svelte-edit-activity-container'),
});
