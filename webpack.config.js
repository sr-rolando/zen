const path = require('path');

module.exports = {
    entry: './assets/zen.js',  // path to our core input file
    output: {
        filename: 'zen-bundle.js',  // output bundle file name
        path: path.resolve(__dirname, './static/js'),  // path to our output directory for static frontend files
    },
};
