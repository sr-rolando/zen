package version // import "zen/version"

import "time"

// Version is defined in the Makefile.
var Version = "<development>"

// BuildDate is set by 'make build'
var BuildDate = time.Now().String()
