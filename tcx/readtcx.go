package tcx // import "zen/tcx"

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)

// The Parser interface provides the oudside view for accessing TCX training data files.
type Parser interface {
	ReadTCX()
	ActivityType() string
	Duration() float64
	StartLatitute() float64
	StartLongitude() float64
}

// File implements our TCXParser interface by reading the given file.
type File struct {
	Path         string
	TrainingData TrainingCenterDatabase
}

// ReadTCX implements our TCXParser interface.
func (tcx *File) ReadTCX() {
	tcxFile, err := os.Open(tcx.Path)
	if err != nil {
		fmt.Println(err)
	}

	// Defer the closing to allow parsing the file.
	defer tcxFile.Close()

	// Read in the file.
	byteValue, _ := ioutil.ReadAll(tcxFile)

	// Store the result for later use.
	tcx.TrainingData, _ = ReadFromByteArray(byteValue)
}

// ReadFromByteArray assumes the byte array to contain valid TXF workout data.
func ReadFromByteArray(byteArray []byte) (TrainingCenterDatabase, error) {
	// Initialise data structure.
	var workout TrainingCenterDatabase

	// Unmarshalling means to read in the XML data into our data structure.
	err := xml.Unmarshal(byteArray, &workout)

	return workout, err
}

// ActivityType returns the one and only activity type ("running" etc.) of the file.
func (tcx *File) ActivityType() string {
	if len(tcx.TrainingData.Activities) < 1 {
		return "No activity"
	}
	if len(tcx.TrainingData.Activities[0].Activities) < 1 {
		return "No activity"
	}
	return tcx.TrainingData.Activities[0].Activities[0].Sport
}

// Duration returns the sum of all laps' durations.
func (tcx *File) Duration() float64 {
	if len(tcx.TrainingData.Activities) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities) < 1 {
		return 0
	}
	var ret = 0.0
	for l := 0; l < len(tcx.TrainingData.Activities[0].Activities[0].Laps); l++ {
		var lap = tcx.TrainingData.Activities[0].Activities[0].Laps[l]
		ret += lap.TotalTimeSeconds
	}
	return ret
}

// StartLatitude provides just this: the latitude of the activity's start location.
func (tcx *File) StartLatitude() float32 {
	if len(tcx.TrainingData.Activities) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities[0].Laps) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities[0].Laps[0].Tracks) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities[0].Laps[0].Tracks[0].Trackpoints) < 1 {
		return 0
	}
	return tcx.TrainingData.Activities[0].Activities[0].Laps[0].Tracks[0].Trackpoints[0].Position.LongitudeDegrees
}

// StartLongitude provides just this: the longitude of the activity's start location.
func (tcx *File) StartLongitude() float32 {
	if len(tcx.TrainingData.Activities) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities[0].Laps) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities[0].Laps[0].Tracks) < 1 {
		return 0
	}
	if len(tcx.TrainingData.Activities[0].Activities[0].Laps[0].Tracks[0].Trackpoints) < 1 {
		return 0
	}
	return tcx.TrainingData.Activities[0].Activities[0].Laps[0].Tracks[0].Trackpoints[0].Position.LongitudeDegrees
}
