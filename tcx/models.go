package tcx // import "zen/tcx"

import (
	"encoding/xml"
	"time"
)

// TrainingCenterDatabase is the root element of a tcx file. Currently, we use this as our model.
type TrainingCenterDatabase struct {
	XMLName    xml.Name     `xml:"TrainingCenterDatabase"`
	Activities []Activities `xml:"Activities"`
}

// Activities normally includes just one activity.
type Activities struct {
	XMLName    xml.Name   `xml:"Activities"`
	Activities []Activity `xml:"Activity"`
}

// The Activity stores data on being active.
type Activity struct {
	XMLName    xml.Name `xml:"Activity"`
	Sport      string   `xml:"Sport,attr"`
	ActivityID string   `xml:"Id"`
	Laps       []Lap    `xml:"Lap"`
	Creator    Creator  `xml:"Creator"`
}

// A Lap usually occurs multiple times per activity (i.e. every minute).
type Lap struct {
	StartTime           time.Time           `xml:"StartTime,attr"`
	TotalTimeSeconds    float64             `xml:"TotalTimeSeconds"`
	DistanceMeters      float64             `xml:"DistanceMeters"`
	MaximumSpeed        float64             `xml:"MaximumSpeed"`
	Calories            int                 `xml:"Calories"`
	AverageHeartRateBpm AverageHeartRateBpm `xml:"AverageHeartRateBpm"`
	MaximumHeartRateBpm MaximumHeartRateBpm `xml:"MaximumHeartRateBpm"`
	Intensity           string              `xml:"Intensity"`
	Cadence             int                 `xml:"Cadence"`
	TriggerMethod       string              `xml:"TriggerMethod"`
	Tracks              []Track             `xml:"Track"`
}

// AverageHeartRateBpm averages the heart beats for a whole lap.
type AverageHeartRateBpm struct {
	Value int `xml:"Value"`
}

// MaximumHeartRateBpm is the maximum of all heart beat measures for a given lap.
type MaximumHeartRateBpm struct {
	Value int `xml:"Value"`
}

// A Track is a short part of a route consisting of trackpoints.
type Track struct {
	Trackpoints []Trackpoint `xml:"Trackpoint"`
}

// Trackpoint is a position in a track.
type Trackpoint struct {
	Time           time.Time    `xml:"Time"`
	Position       Position     `xml:"LongitudeDegrees"`
	AltitudeMeters float32      `xml:"AltitudeMeters"`
	DistanceMeters float32      `xml:"DistanceMeters"`
	HeartRateBpm   HeartRateBpm `xml:"HeartRateBpm"`
	Cadence        int          `xml:"Cadence"`
	SensorState    string       `xml:"SensorState"`
}

// Position defines a geo coordinate.
type Position struct {
	LatitudeDegrees  float32 `xml:"LatitudeDegrees"`
	LongitudeDegrees float32 `xml:"LongitudeDegrees"`
}

// The HeartRateBpm defines the heart beats per second for a specific track point.
type HeartRateBpm struct {
	Value int `xml:"Value"`
}

// The Creator holds the gadget (i.e. watch) used to measure the workout.
type Creator struct {
	Name string `xml:"Name"`
}
