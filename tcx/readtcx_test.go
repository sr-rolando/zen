package tcx

import "testing"

func TestZenWorkout(t *testing.T) {
	want := "Running"
	tcx := File{Path: "../testdata/hoheduene_strand.tcx"}
	tcx.ReadTCX()
	got := tcx.ActivityType()
	if got != want {
		t.Errorf("File() = %q, want %q", got, want)
	}
}
