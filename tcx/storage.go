//
// Handles all database related methods for tcx files.
//
package tcx // import "zen/tcx"

import (
	"context"
	"fmt"
	"math"
	"time"

	"github.com/jackc/pgx/v4"
)

// PersistInDb takes a TCX dataset and persists it as entity 'activity'.
// Returns ID of activity in the database, always > 0 on success, always the latest in case of multiple uploads.
func PersistInDb(db *pgx.Conn, tcxRoot TrainingCenterDatabase) (int64, error) {
	// Return this on error, along with respective error message.
	var invalidActivityId int64
	invalidActivityId = -1

	var mostRecentActivityId int64

	if db == nil {
		return invalidActivityId, fmt.Errorf("cannot persist TCX data as database connection is not available")
	}
	// Start transaction.
	tx, err := db.Begin(context.Background())
	if err != nil {
		return invalidActivityId, fmt.Errorf("tcx storage: unable to start transaction: %v", err)
	}

	// The activities are nested. Usually, there is only one 'outer' <Activities> entry.
	for idxActivityOuter := 0; idxActivityOuter < len(tcxRoot.Activities); idxActivityOuter++ {
		for idxActivity := 0; idxActivity < len(tcxRoot.Activities[idxActivityOuter].Activities); idxActivity++ {
			tcxActivity := tcxRoot.Activities[idxActivityOuter].Activities[idxActivity]

			// Check kind of sport that this activity represents. Create it if it doesn't exist, yet.
			var sportId int
			err = tx.QueryRow(context.Background(), "SELECT id FROM sport WHERE name = $1", tcxActivity.Sport).Scan(&sportId)
			if err != nil {
				// If the sport doesn't exist yet, create it.
				sql := "INSERT INTO sport (name) VALUES ($1) RETURNING id"
				err = tx.QueryRow(context.Background(), sql, tcxActivity.Sport).Scan(&sportId)
				if err != nil {
					tx.Rollback(context.Background())
					return invalidActivityId, fmt.Errorf("sport '%s' couldn't be created, error: %v", tcxActivity.Sport, err)
				}
			}

			// Get some default sport type which can later be changed by the user.
			var sportTypeId int
			sql := "SELECT id FROM sport_type WHERE sport_id = $1 ORDER BY id LIMIT 1"
			err = tx.QueryRow(context.Background(), sql, sportId).Scan(&sportTypeId)
			if err != nil {
				sql = "INSERT INTO sport_type (sport_id, name, abbr) VALUES ($1, $2, $3) RETURNING id"
				err = tx.QueryRow(context.Background(), sql, sportId, "Standard", "STD").Scan(&sportTypeId)
				if err != nil {
					tx.Rollback(context.Background())
					return invalidActivityId, fmt.Errorf("sport type 'Standard' couldn't be created, error: %v", err)
				}
			}

			gearId, err := getOrCreateDefaultGear(tx)
			if err != nil {
				tx.Rollback(context.Background())
				return invalidActivityId, err
			}

			// Create the activity.
			var activityId int64
			sql = "INSERT INTO activity (sport_id, sport_type_id, gear_id, start_time, end_time) VALUES ($1, $2, $3, $4, $5) RETURNING id"
			err = tx.QueryRow(context.Background(), sql, sportId, sportTypeId, gearId, time.Now(), time.Now()).Scan(&activityId)
			if err != nil {
				tx.Rollback(context.Background())
				return invalidActivityId, fmt.Errorf("inserting into activity failed: %v", err)
			}

			// The activity's laps.
			var startTime time.Time
			var endTime time.Time
			var distance float64
			for idxLap := 0; idxLap < len(tcxActivity.Laps); idxLap++ {
				lap := tcxActivity.Laps[idxLap]

				// Start and end time.
				lapStartTime := lap.StartTime
				lapDuration, _ := time.ParseDuration(fmt.Sprintf("%fs", lap.TotalTimeSeconds))
				lapEndTime := lapStartTime.Add(lapDuration)
				if startTime.IsZero() || lapStartTime.Before(startTime) {
					startTime = lapStartTime
				}
				if endTime.IsZero() || lapEndTime.After(endTime) {
					endTime = lapEndTime
				}

				// Distance in meters.
				lapDistance := lap.DistanceMeters
				distance += lapDistance

				lapCalories := lap.Calories

				// The minimum heart rate is determined by means of all trackpoints in this lap.
				lapHeartRateMin := math.MaxInt16

				// Create the lap entity, keep its id.
				var lapId int
				sql := "INSERT INTO lap (activity_id, start_time, duration, distance, calories, heart_rate_min) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
				err := tx.QueryRow(context.Background(), sql, activityId, lapStartTime, lapDuration, lapDistance, lapCalories, lapHeartRateMin).Scan(&lapId)
				if err != nil {
					tx.Rollback(context.Background())
					return invalidActivityId, fmt.Errorf("inserting into lab failed: %v", err)
				}

				// The tracks and trackpoints.
				for idxTrack := 0; idxTrack < len(lap.Tracks); idxTrack++ {
					track := lap.Tracks[idxTrack]
					// Create the track, keep its id.
					var trackId int
					sql = "INSERT INTO track (lap_id) VALUES($1) RETURNING id"
					err = tx.QueryRow(context.Background(), sql, lapId).Scan(&trackId)
					if err != nil {
						tx.Rollback(context.Background())
						return invalidActivityId, fmt.Errorf("inserting into track failed: %v", err)
					}

					// All a track has is trackpoints.
					for idxTrackpoint := 0; idxTrackpoint < len(track.Trackpoints); idxTrackpoint++ {
						tpoint := track.Trackpoints[idxTrackpoint]
						// Time (should always be there, regardless of kind of sport).
						tpTime := tpoint.Time

						// Create trackpoint, id of no interest. Automatically stores 0-values for empty elements.
						sql = "INSERT INTO trackpoint (track_id, time_stamp, latitude, longitude, altitude, distance, heart_rate) VALUES($1, $2, $3, $4, $5, $6, $7)"
						_, err := tx.Exec(context.Background(), sql,
							trackId, tpTime, tpoint.Position.LatitudeDegrees, tpoint.Position.LongitudeDegrees,
							tpoint.AltitudeMeters, tpoint.DistanceMeters, tpoint.HeartRateBpm.Value)
						if err != nil {
							tx.Rollback(context.Background())
							return invalidActivityId, fmt.Errorf("inserting into trackpoint failed: %v", err)
						}
					}
				}
			}

			// Update activity's start and end time based on laps data.
			sql = "UPDATE activity SET start_time = $1, end_time = $2 WHERE ID = $3"
			_, err = tx.Exec(context.Background(), sql, startTime, endTime, activityId)
			if err != nil {
				tx.Rollback(context.Background())
				return invalidActivityId, fmt.Errorf("updating start_time / end_time of activity failed: %v", err)
			}
			mostRecentActivityId = activityId
		}
	}

	// Be done
	if err := tx.Commit(context.Background()); err != nil {
		return invalidActivityId, fmt.Errorf("error committing transaction: %v", err)
	}

	return mostRecentActivityId, nil
}

func getOrCreateDefaultGear(tx pgx.Tx) (int, error) {
	// Get the default gear type first.
	var gearTypeId int
	sql := "SELECT id FROM gear_type ORDER BY id LIMIT 1"
	err := tx.QueryRow(context.Background(), sql).Scan(&gearTypeId)
	if err != nil {
		sql = "INSERT INTO gear_type (name) VALUES ($1) RETURNING id"
		err = tx.QueryRow(context.Background(), sql, "No gear").Scan(&gearTypeId)
		if err != nil {
			tx.Rollback(context.Background())
			return 0, fmt.Errorf("default gear type 'No gear' couldn't be created, error: %v", err)
		}
	}

	// Now, for this default gear type, get or create the default dummy gear.
	var gearId int
	sql = "SELECT id FROM gear WHERE gear_type_id = $1 and decommissioned = false ORDER BY id LIMIT 1"
	err = tx.QueryRow(context.Background(), sql).Scan(&gearId)
	if err != nil {
		sql = "INSERT INTO gear (gear_type_id, name, brand) VALUES ($1, $2, $3) RETURNING id"
		err = tx.QueryRow(context.Background(), sql, gearTypeId, "No gear", "unbranded").Scan(&gearId)
		if err != nil {
			tx.Rollback(context.Background())
			return 0, fmt.Errorf("default gear 'No gear' couldn't be created, error: %v", err)
		}
	}
	return gearId, nil
}
