create table sport (
    id serial primary key,
    name text not null
);
comment on table sport IS 'Running or Biking or Swimming.';

create table sport_type (
    id serial primary key,
    sport_id integer references sport(id) not null,
    name text not null,
    abbr text not null
);
comment on table sport_type IS 'Interval run / IR, Long run / LR, etc.';

create table gear_type (
    id serial primary key,
    name text not null,
    default_max_km integer default 1000
);
comment on table gear_type is 'This type includes shoes, shirt, bike and the like.';

create table gear (
    id serial primary key,
    gear_type_id integer references gear_type(id) not null,
    name text not null,
    brand text not null,
    initial_km integer not null default 0 not null,
    use_start date default now(),
    use_end date default now(),
		decommissioned boolean not null default false
);
comment on table gear is 'Specific gear, i.e. a pair of shoes.';
comment on column gear.use_end is 'Only of relevance if decommissioned is true.';

create table activity (
    id serial primary key,
    sport_id integer references sport(id) not null,
    sport_type_id integer references sport_type(id) not null,
    gear_id integer references gear(id) not null,
    title text default '',
    start_time timestamp with time zone not null,
    end_time timestamp with time zone not null,
    distance real default 0.0,
    pace real default 0.0,
    pace_min real default 0.0,
    pace_max real default 0.0,
    heart_rate_min smallint default 0,
    heart_rate_max smallint default 0,
    heart_rate_avg smallint default 0,
    creator text not null default ''
);
comment on table activity is 'Holds sport activities.';
comment on column activity.gear_id is 'We settle for assigning a single pieace of gear to an activity. For now.';
comment on column activity.creator is 'Device used to record the activity.';
comment on column activity.distance is 'Distance in meters as calculated by latitudes, longitudes and possibly elevation.';
comment on column activity.pace is 'Overall pace for the whole activity.';
comment on column activity.pace_min is 'Pace for the slowest lap of the activity.';
comment on column activity.pace_max is 'Maximum pace in any lap of the activity.';

create table lap (
    id serial primary key,
    activity_id integer references activity(id) not null,
    start_time timestamp with time zone not null,
    duration interval not null,
    distance real default 0.0,
    calories integer default 0,
    heart_rate_min smallint default 0,
    heart_rate_max smallint default 0,
    heart_rate_avg smallint default 0
);
comment on table lap is 'Each activity consists of eat least one lap.';
comment on column lap.distance is 'Distance in meters.';

create table track (
    id serial primary key,
    lap_id integer references lap(id) not null
);
comment on table track is 'A lap has at least one track.';

create table trackpoint (
    id bigserial primary key,
    track_id integer references track(id) not null,
    time_stamp timestamp with time zone not null,
    latitude decimal default -1.0,
    longitude decimal default -1.0,
    altitude real default 0.0,
    distance real default 0.0,
    heart_rate smallint default 0
);
comment on table trackpoint is 'A track has usually multiple trackpoints.';
comment on column trackpoint.altitude is 'Altitude in meters.';
comment on column trackpoint.distance is 'Distance in meters as recorded by the device.';
