package database // import "zen/database"

import (
	"context"
	"fmt"
	"log"

	// The database driver. We use this instead of "database/sql" as we only target PostgreSQL.
	"github.com/jackc/pgx/v4"
)

// OpenDb initiates access to our database.
func OpenDb(dsn string) (*pgx.Conn, error) {
	conn, err := pgx.Connect(context.Background(), dsn)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

// PrintDbVersion is validating that our global DB pointer is working. Also console-prints DBMS version.
func PrintDbVersion(db *pgx.Conn) {
	var dbms string
	err := db.QueryRow(context.Background(), "select version()").Scan(&dbms)
	if err != nil {
		log.Println("Error: ", err)
		return
	}
	log.Println("Connected to:", dbms)
}

// EnsureSchemaIsUpToDate makes sure the DB schema is up to date based on the migration files available in migrations.go.
func EnsureSchemaIsUpToDate(db *pgx.Conn) error {
	var currentSchemaVersion int
	err := db.QueryRow(context.Background(), "SELECT version FROM schema_version").Scan(&currentSchemaVersion)
	if err != nil {
		// At the initial run, we don't even have the schema_version table.
		// The first migration fixes this.
		err = applyOneMigration(db, 0, 1)
		if err != nil {
			return fmt.Errorf("Running initial database migration failed with %v", err)
		}

		// Try again.
		err = db.QueryRow(context.Background(), "SELECT version FROM schema_version").Scan(&currentSchemaVersion)
		if err != nil {
			return fmt.Errorf("Validating current database schema version failed with %v", err)
		}
	}

	// availableSchemaVersion: defined in migrations.go
	if currentSchemaVersion < availableSchemaVersion {
		log.Printf("Migrationg database from version %d to %d...", currentSchemaVersion, availableSchemaVersion)

		for version := currentSchemaVersion; version < availableSchemaVersion; version++ {
			newVersion := version + 1
			applyOneMigration(db, version, newVersion)
		}
	} else {
		log.Printf("Database schema version %d is up to date.", currentSchemaVersion)
	}

	return nil
}

// Perform a single migration run.
func applyOneMigration(db *pgx.Conn, currentVersion, newVersion int) error {
	fmt.Println("-> migrating to version:", newVersion)

	if db == nil {
		return fmt.Errorf("  - we don't have no valid database connection")
	}

	// Start transaction.
	tx, err := db.Begin(context.Background())
	if err != nil {
		return fmt.Errorf("[Migration v%d] %v", newVersion, err)
	}

	// Run migration which is defined as element of the 0-based array in migrations.go.
	if err := migrations[currentVersion](tx); err != nil {
		tx.Rollback(context.Background())
		return fmt.Errorf("[Migration v%d] %v", newVersion, err)
	}

	// Update the one and only entry in table schema_version.
	if _, err := tx.Exec(context.Background(), "DELETE FROM schema_version"); err != nil {
		tx.Rollback(context.Background())
		return fmt.Errorf("[Migration v%d] %v", newVersion, err)
	}
	if _, err := tx.Exec(context.Background(), "INSERT INTO schema_version (version) VALUES ($1)", newVersion); err != nil {
		tx.Rollback(context.Background())
		return fmt.Errorf("[Migration v%d] %v", newVersion, err)
	}

	// Commit transaction for this migration.
	if err := tx.Commit(context.Background()); err != nil {
		return fmt.Errorf("[Migration v%d] %v", newVersion, err)
	}

	return nil
}

// ShutdownDb closes any database connections.
func ShutdownDb(db *pgx.Conn) {
	if db != nil {
		db.Close(context.Background())
	}
}
