package activities // import "activities/database"

import (
  "context"
	"log"
  "time"

	"github.com/jackc/pgx/v4"
)

// ReadActivities retrieves the `count` most current activities from the database.
func ReadActivities(db *pgx.Conn, count int) ([]ZenActivity, error) {
	temp, _ := db.Query(context.Background(), "select * from activity")
	log.Printf(" ..overall = %d entries in activities", temp.CommandTag().RowsAffected())
	temp.Next()
	log.Printf(" ..overall error: %v", temp.Err())
	temp.Close()

  sql := "SELECT id, title, start_time, end_time, distance, pace, heart_rate_max FROM activity ORDER BY start_time desc"
  rows, err := db.Query(context.Background(), sql)
	if err != nil {
		log.Printf(" ..error retrieving activities from db: %v.", err)
	}
	log.Printf("  ..got %d activities from db.", rows.CommandTag().RowsAffected())
	res := []ZenActivity{}
  for rows.Next() {
		if err := rows.Err(); err != nil {
			log.Printf("  ..error retrieving activity from db: %v\n", err)
			continue
		}
    var id int64
    var title string
    var start_time time.Time
    var end_time time.Time
    var distance float64
    var pace float32
    var heart_rate_max int16
    err := rows.Scan(&id, &title, &start_time, &end_time, &distance, &pace, &heart_rate_max)
    // Skip current activity if we see an error.
    if err != nil {
      log.Printf("  ..error reading activity => %v", err)
      continue
    }
    activity := ZenActivity{
      ID: id,
      Title: title,
      StartTime: start_time,
      EndTime: end_time,
      DistanceMeters: distance,
      Pace: pace,
      HRmax: heart_rate_max,
    }
	  res = append(res, activity)
  }
	return res, nil
}
