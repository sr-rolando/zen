package activities // import "activities/models"

import (
  "time"
)

// ZenActivity is an application model class that is only a subset
// of what we store in the activity database table. Used in the dashboard.
type ZenActivity struct {
	ID int64
  Title string
  StartTime time.Time
  EndTime time.Time
  DistanceMeters float64
  Pace float32
  HRmax int16
}
