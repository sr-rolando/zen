package main

//go:generate go run generate.go

import (
	"zen/config"
	"zen/httpd"
)

func main() {
	dsn, err := config.DatabaseURI()
	panicOnError(err)
	if dsn == "" {
		panic("We can't connect to an empty data source name. Please check your zen.toml config")
	}

	httpd.RunServer(dsn)
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
