// Package config provides acces to the settings defined in zen.toml.
package config // import "zen/config"

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/pelletier/go-toml"
)

// Postgres defines all config settings required for connecting to our database.
type Postgres struct {
	DbUser     string
	DbPassword string
	DbName     string
	DbHost     string
	DbPort     int
}

// Config provides access to all configuration settings defined in zen.toml.
type Config struct {
	Postgres Postgres
}

// DatabaseURI provides the ready-to-use connection string for our database.
func DatabaseURI() (string, error) {
	configfile, err := os.Open("zen.toml")
	if err != nil {
		log.Print("Error reading zen.toml config file: ", err)
	}
	defer configfile.Close()

	// Read in the file.
	byteValue, _ := ioutil.ReadAll(configfile)
	config := Config{}
	toml.Unmarshal(byteValue, &config)

	err = checkConfigIsComplete(config)
	if err != nil {
		return "", err
	}

	// Format: https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.Postgres.DbHost, config.Postgres.DbPort,
		config.Postgres.DbUser, config.Postgres.DbPassword,
		config.Postgres.DbName)
	return dsn, nil
}

func checkConfigIsComplete(config Config) error {
	if config.Postgres.DbUser == "" {
		return fmt.Errorf("zen.toml config file does not seem to contain Postgres.DbUser setting")
	}
	if config.Postgres.DbPassword == "" {
		return fmt.Errorf("zen.toml config file does not seem to contain Postgres.DbPassword setting")
	}
	if config.Postgres.DbName == "" {
		return fmt.Errorf("zen.toml config file does not seem to contain Postgres.DbName setting")
	}
	if config.Postgres.DbHost == "" {
		return fmt.Errorf("zen.toml config file does not seem to contain Postgres.DbHost section")
	}
	if config.Postgres.DbPort == 0 {
		return fmt.Errorf("zen.toml config file does not seem to contain Postgres.DbPort section")
	}
	return nil
}
