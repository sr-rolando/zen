APP := zen
VERSION := "0.2.0"
BUILD_DATE := `date +%FT%T%z`

# -s -w: omit debug and similar symbols.
LD_FLAGS := "-s -w -X 'zen/version.Version=$(VERSION)' -X 'zen/version.BuildDate=$(BUILD_DATE)'"

PKG_LIST := $(shell find . -type f -name "*.go" | grep -v polaraccesslink)

.PHONY: clean \
	test \
	run \
	lint

# Starts at generate.go. Also processes all occurrances of //go:generate
generate:
	@ go generate

# Installs frontend components required for Svelte
install-fe:
	cd svelte-components && pnpm install

build: install-fe generate $(PKG_LIST)
	@ cd svelte-components && pnpm run build
	@ go build -o $(APP) -ldflags=$(LD_FLAGS) main.go

zen: build

# Debug build
debug: $(PKG_LIST)
	@ go build -o $(APP) main.go

# This keeps running in foreground, watching for changes in frontend code,
# allowing debug infos therein.
run-fe:
	@ cd svelte-components && pnpm run dev

run: generate
	@ echo "Please run 'make run-fe' in a separate shell."
	@ go run main.go -debug

clean:
	@ go clean -modcache
	@ rm -f $(APP)

test:
	go test -cover -race -count=1 ./...

lint:
	@ cd svelte-components && npx svelte-check
	@ golint -set_exit_status $(PKG_LIST)
